# Anscombe Quartet

Jupyter Notebook to use in describing and discussing [Anscombe's Quartet](https://en.wikipedia.org/wiki/Anscombe%27s_quartet) in a classroom context.